
#  Copyright (C) 2021. Logiciel élaboré par l'État, via la Drees.

#  Nom de l'auteur : Albane Miron de l'Espinay, Drees.

#  Ce programme informatique a été développé par la Drees. Il permet de produire les tableaux et graphiques publiés dans le Dossier de la Drees "La vaccination contre la Covid-19 en Ehpad et son effet sur la contamination des résidents", novembre 2021.

# Ce programme a été exécuté le 09/11/2021 avec la version 3.6.1 de R et les packages data.table V.1.13.0, plotly v4.9.2.1, ggplot2 v3.3.3, stringr v1.4.0, dplyr v1.0.6

# Le texte et les tableaux de l'article peuvent être consultés sur le site de la DREES https://drees.solidarites-sante.gouv.fr/publications/les-dossiers-de-la-drees/la-vaccination-contre-la-covid-19-en-ehpad-et-son-effet-sur

#  Ce programme mobilise des données agrégées du système d'information sur la vaccination (Vac-SI, CNAM) et les données brutes de l'enquête Covid-ESMS de Santé Publique France, à l'échelle des établissements.

#  Bien qu'il n'existe aucune obligation légale à ce sujet, les utilisateurs de ce programme sont invités à signaler à la DREES leurs travaux issus de la réutilisation de ce code, ainsi que les éventuels problèmes ou anomalies qu'ils y rencontreraient, en écrivant à DREES-CODE@sante.gouv.fr


# Ce logiciel est régi par la licence "GNU General Public License" GPL-3.0. # https://spdx.org/licenses/GPL-3.0.html#licenseText

# À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

# Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence GPL-3.0, et que vous en avez accepté les termes.



# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.If not, see <https://www.gnu.org/licenses/>.



library(plotly)

# Nombre d'épisodes en cours 
nb_episodes_en_cours <- function(dt_){
  nep <- dt_[, .(nb_episodes = sum(indic_episode_en_cours))]$nb_episodes
  return(list(nombre = nep, taux=round(100*nep/nrow(dt_))))
}


# Nombre d'épisodes en cours avec au moins un résident cas confirmé 
# En fait, ce n'est que ces épisodes qui nous interessent ? 

nb_episodes_resident <- function(dt_){
  nep <- dt_[nb_conf_res_max >0 , .(nb_episodes = sum(indic_episode_en_cours))]$nb_episodes
  return(list(nombre = nep, taux=round(100*nep/nrow(dt_))))
}


# Pour chaque tranche de vaccination **à la date d'ajd**, quel pourcentage d'épisodes en cours ? 
# Le nombre de résidents est pris égal au nombre indiqué lors du dernier épisode

quote.convert <- function(x) eval(parse(text=paste0('quote(',x,')')))


# pas d'épisode en cours si date de début des signes du premier cas il y a plus de njours_excl_episodes jours
repart_tranche_vacc_resident <- function(dt0_, excl_pub_hosp = FALSE, njours_excl_episodes = 1000,
                                         col_interet = "cum_doses_1_65_et_plus_datesituation", 
                                         restriction_ep_en_cours = FALSE, restriction_ep_en_cours_residents = FALSE,
                                         excl_plus_100 = FALSE,
                                         restriction_episodes_confirmes = FALSE){
  
  dt_ <- copy(dt0_)
  
  if (excl_pub_hosp) {dt_ <- dt_[statut_jur_lib != "Public hospitalier"]}
  
  dt_[, indic_episode_en_cours_recent := indic_episode_en_cours]
  if (restriction_episodes_confirmes) {
    dt_[indic_episode_en_cours == 1, indic_episode_en_cours_recent := ifelse((nb_conf_res_max > 0 | nb_conf_perso_max > 0), 1, 0)]
    print(dt_)
  }
  
  dt_[, longueur_episode := as.Date(extract_date) - as.Date(date_deb_sign_premcasc_ep)]
  
  dt_[longueur_episode > njours_excl_episodes, indic_episode_en_cours_recent := 0]
  dt_[, indic_vieil_episode := indic_episode_en_cours - indic_episode_en_cours_recent]
  
  
  if(restriction_ep_en_cours) {dt_ <- dt_[indic_episode_en_cours_recent == 1]}
  if(restriction_ep_en_cours_residents) {dt_ <- dt_[nb_conf_res_max>0]}
  
  varname <- quote.convert(col_interet)
  col_res <- paste0("tranche_", col_interet)
  dt_[, `:=`(taux_vac = eval(varname)/nb_residents_total)]
  
  dt_[, (col_res) := cut(taux_vac, c(-0.01, 0, seq(0.2, 1, 0.2), 50), right = TRUE)]
  
  if(grepl("cum_doses_2", col_interet)) {
    dt_[, (col_res) := cut(taux_vac, c(-0.01, 0, 0.5, 1, 50), right = TRUE)]
  }
  
  if(excl_plus_100){dt_ <- dt_[taux_vac <=1]}
  
  res <- dt_[, .(part_ep_en_cours_recent = 
                   round(100*sum(indic_episode_en_cours_recent)/(.N - sum(indic_vieil_episode)), 1),
                 part_ep_en_cours_recent_resident = 
                   round(100*sum(indic_episode_en_cours_recent*(nb_conf_res_max>0), na.rm = T)/(.N - sum(indic_vieil_episode)), 1),
                 part_residents_confirmes_episode_selec = 
                   round(100*sum(nb_conf_res_max*indic_episode_en_cours_recent, na.rm = T) / sum(nb_residents_total*(1-indic_vieil_episode)), 1),
                 part_residents_dcd_episode_selec = 
                   round(100*sum(nb_residents_dcd_tot*indic_episode_en_cours_recent, na.rm = T) / sum(nb_residents_total*(1-indic_vieil_episode)), 2),
                 
                 nombre_ehpad_inclus = .N - sum(indic_vieil_episode),
                 nombre_res_inclus = sum(nb_residents_total*(1-indic_vieil_episode)),
                 nombre_vieux_episodes = sum(indic_vieil_episode),
                 nombre_episode_recent = sum(indic_episode_en_cours_recent),
                 nombre_res_episode_recent = sum(indic_episode_en_cours_recent*nb_residents_total)), by=col_res]
  
  res_longueur <- dt_[indic_episode_en_cours_recent == 1, 
                      .(longueur_moy_episode = mean(longueur_episode, na.rm = TRUE),
                        longueur_mediane_episode = median(longueur_episode, na.rm = TRUE)), by=col_res]
  
  res <- merge(res, res_longueur)
  
  res_total <- dt_[, .(part_ep_en_cours_recent = 
                         round(100*sum(indic_episode_en_cours_recent)/(.N - sum(indic_vieil_episode)), 1),
                       part_ep_en_cours_recent_resident = 
                         round(100*sum(indic_episode_en_cours_recent*(nb_conf_res_max>0), na.rm = T)/(.N - sum(indic_vieil_episode)), 1),
                       part_residents_confirmes_episode_selec = 
                         round(100*sum(nb_conf_res_max*indic_episode_en_cours_recent, na.rm = T) / sum(nb_residents_total*(1-indic_vieil_episode)), 1),
                       part_residents_dcd_episode_selec = 
                         round(100*sum(nb_residents_dcd_tot*indic_episode_en_cours_recent, na.rm = T) / sum(nb_residents_total*(1-indic_vieil_episode)), 2),
                       
                       nombre_ehpad_inclus = .N - sum(indic_vieil_episode),
                       nombre_res_inclus = sum(nb_residents_total*(1-indic_vieil_episode)),
                       nombre_vieux_episodes = sum(indic_vieil_episode),
                       nombre_episode_recent = sum(indic_episode_en_cours_recent),
                       nombre_res_episode_recent = sum(indic_episode_en_cours_recent*nb_residents_total))]
  
  res_total <- bind_cols(res_total, dt_[indic_episode_en_cours_recent == 1, 
                                        .(longueur_moy_episode = mean(longueur_episode, na.rm = TRUE),
                                          longueur_mediane_episode = median(longueur_episode, na.rm = TRUE))])
  
  res_total[, (col_res) := "Ensemble"]
  
  res <- bind_rows(res, res_total)
  
  res[, part_des_ep_avec_res_conf := round(100*part_ep_en_cours_recent_resident/part_ep_en_cours_recent, 1)]
  
  res <- res[!is.na(eval(quote.convert(col_res)))]
  res <- res[order(eval(quote.convert(col_res)))]
  
  res <- mise_en_forme_tranches(res)
  
  return(res)
}

mise_en_forme_stat <- function(dt_stat) {
  
  cols_sel <- c(colnames(dt_stat)[1],"nombre_ehpad_inclus", "nombre_res_inclus", "nombre_episode_recent",  "part_ep_en_cours_recent", "part_des_ep_avec_res_conf", "longueur_mediane_episode")
  
  dt_stat <- dt_stat[, ..cols_sel]
  
  if (any(grepl("doses_1", colnames(dt_stat)))){
    new_lib <- "Part de résidents ayant reçu la première dose il y a plus de 14 jours"}
  if (any(grepl("doses_2", colnames(dt_stat)))){
    new_lib <- "Part de résidents ayant reçu la seconde dose il y a plus de 14 jours"}
  
  cols_lib <- c(new_lib,"Nombre total d'Ehpad inclus", 
                "Nombre total de résidents inclus",
                "Nombre d'Ehpad où un épisode a débuté durant les 14 derniers jours", 
                "Pourcentage d'Ehpad où un épisode a débuté durant les 14 derniers jours",
                "Pourcentage de ces épisodes ou au moins un résident est cas confirmé",
                "Médiane du nombre de jours écoulés depuis le début de l'épisode"
  )
  
  setnames(dt_stat, cols_sel, cols_lib)
  
  return(dt_stat)
  
}

mise_en_forme_atteinte <- function(dt_att) {
  
  
  if (any(grepl("doses_1", colnames(dt_att)))){
    new_lib <- "Part de résidents ayant reçu la première dose 14 jours avant le début de l'épisode"}
  if (any(grepl("doses_2", colnames(dt_att)))){
    new_lib <- "Part de résidents ayant reçu la seconde dose avant le début de l'épisode"}
  
  list_sel <- c(tri = colnames(dt_att)[1], 
                "Nombre d'Ehpad où un épisode a débuté durant les 14 derniers jours" = "nombre_episode_recent",  
                "Nombre de résidents dans les Ehpad où un épisode a débuté durant les 14 derniers jours" = "nombre_res_episode_recent",
                "Pourcentage des épisodes ou au moins un résident est cas confirmé" = "part_des_ep_avec_res_conf",
                "Pourcentage de résidents cas confirmés" ="part_residents_confirmes_episode_selec", 
                "Pourcentage de résidents décédés" = "part_residents_dcd_episode_selec", 
                "Médiane du nombre de jours écoulés depuis le début de l'épisode" = "longueur_mediane_episode")
  
  names(list_sel)[1] <- new_lib
  raw_cols <- unname(unlist(list_sel))
  dt_att <- dt_att[, ..raw_cols]
  
  setnames(dt_att, unname(unlist(list_sel)), names(list_sel))
  
  return(dt_att)
}

mise_en_forme_tranches <- function(dt_res_){
  
  dt_res_[dt_res_ == "(-0.01,0]"] <- "Aucune injection"
  dt_res_[dt_res_ == "(0,0.2]"] <- "Entre 0 % et 20 %"
  dt_res_[dt_res_ == "(0.2,0.4]"] <- "Entre 20 % et 40 %"
  dt_res_[dt_res_ == "(0.4,0.6]"] <- "Entre 40 % et 60 %"
  dt_res_[dt_res_ == "(0.6,0.8]"] <- "Entre 60 % et 80 %"
  dt_res_[dt_res_ == "(0.8,1]"] <- "Entre 80 % et 100 %"
  dt_res_[dt_res_ == "(1,50]"] <- "Strictement plus de 100 %"
  
  
  dt_res_[dt_res_ == "(0,0.5]"] <- "Entre 0 % et 50 %"
  dt_res_[dt_res_ == "(0.5,1]"] <- "Entre 50 % et 100 %"
  
  dt_res_[dt_res_ == "(0,0.25]"] <- "Entre 0 % et 25 %"
  dt_res_[dt_res_ == "(0.25,0.5]"] <- "Entre 25 et 50 %"
  dt_res_[dt_res_ == "(0.5,0.75]"] <- "Entre 50 % et 75 %"
  dt_res_[dt_res_ == "(0.75,1]"] <- "Entre 75 % et 100 %"
  
  return(dt_res_)
  
}

gen_hist <- function(dt1_, date_situation_, excl_pub_hosp_ = TRUE) {
  dt_hist1 <- repart_tranche_vacc_resident(dt1_, excl_pub_hosp = excl_pub_hosp_, 
                                           njours_excl_episodes = 10000, 
                                           col_interet = "cum_doses_1_65_et_plus_datesituation",
                                           excl_plus_100 = FALSE)
  
  p1 <- plot_ly(data=dt_hist1[tranche_cum_doses_1_65_et_plus_datesituation != "Ensemble"])%>%
    add_trace(x=~tranche_cum_doses_1_65_et_plus_datesituation, 
              y=~nombre_ehpad_inclus, 
              type="bar", name="Nombre d'EHPAD", 
              hovertext = ~paste0("Soit ", round(100*nombre_ehpad_inclus/sum(nombre_ehpad_inclus)), "% du champ"), 
              marker = list(color = 'rgb(158,202,111)'))%>%
    layout(title = paste0("Nombre d'Ehpad par taux de résidents ayant reçu la première injection 
              avant le ", date_situation_),
           xaxis = list(title = "Pourcentage de résidents ayant reçu une dose"),
           yaxis = list(title = "Nombre d'Ehpad")
    )
  
  dt_hist2 <- repart_tranche_vacc_resident(dt1_, excl_pub_hosp = excl_pub_hosp_, 
                                           njours_excl_episodes = 10000, 
                                           col_interet = "cum_doses_2_65_et_plus_datesituation",
                                           excl_plus_100 = FALSE)
  
  p2 <- plot_ly(data=dt_hist2[tranche_cum_doses_2_65_et_plus_datesituation != "Ensemble"])%>%
    add_trace(x=~tranche_cum_doses_2_65_et_plus_datesituation, 
              y=~nombre_ehpad_inclus, 
              type="bar", name="Nombre d'EHPAD", 
              hovertext = ~paste0("Soit ", round(100*nombre_ehpad_inclus/sum(nombre_ehpad_inclus)), "% du champ"), 
              marker = list(color = 'rgb(158,202,111)'))%>%
    layout(title = paste0("Nombre d'Ehpad par taux de résidents ayant reçu la seconde injection 
              avant le ", date_situation_),
           xaxis = list(title = "Pourcentage de résidents ayant reçu deux doses"),
           yaxis = list(title = "Nombre d'Ehpad")
    )
  
  return(list(inj1 = list(data=dt_hist1, plot=p1), inj2=list(data=dt_hist2, plot=p2)))
}

gen_graphique <- function(dte) {
  
  dt_agg <- dte[, .(nb_episodes = .N,
                    moy_part_vacc = mean(taux_partiellement_vacc),
                    moy_2_moins14 = mean(taux_2_doses_moins14),
                    moy_1 = mean(taux_1_dose),
                    moy_1_moins14 = mean(taux_1_dose_moins14),
                    moy_res_conf = mean(nb_conf_res_max),
                    moy_res_dcd = mean(nb_residents_dcd_tot),
                    # moy_perso_conf = mean(nb_conf_perso_max),
                    moy_residents = mean(nb_es_res_total)), by=.(tranche_cum_doses_2_65_et_plus_debsignpremcas_14)]
  
  dt_agg <- dt_agg[order(tranche_cum_doses_2_65_et_plus_debsignpremcas_14)]
  dt_agg[, tranche_cum_doses_2_65_et_plus_debsignpremcas_14 := as.character(tranche_cum_doses_2_65_et_plus_debsignpremcas_14)]
  dt_agg <- mise_en_forme_tranches(dt_agg)
  
  p <- plot_ly(data = dt_agg[order(tranche_cum_doses_2_65_et_plus_debsignpremcas_14)]) %>%
    add_trace(x=~tranche_cum_doses_2_65_et_plus_debsignpremcas_14, y=~moy_res_conf)%>%
    layout(title = "Nombre de résidents cas confirmés par tranche vaccinale (2ème injection)",
           xaxis=list(title = "Part de résidents vaccinés"),
           yaxis=list(title = "Nombre moyen de résidents cas confirmés")) %>%
    add_annotations(x = ~tranche_cum_doses_2_65_et_plus_debsignpremcas_14,
                    y = ~moy_res_conf,
                    text = ~paste0(sub("\\.", ",", round(moy_res_conf, 1)) , 
                                   " résidents infectés <br> en moyenne pour les <br>", 
                                   nb_episodes, 
                                   " épisodes signalés"),
                    xref = "x",
                    yref = "y",
                    showarrow = TRUE,
                    # arrowhead = 4,
                    # arrowsize = .5,
                    ax = 0,
                    ay = -30)
  
  p1 <- plot_ly(data = dt_agg[order(tranche_cum_doses_2_65_et_plus_debsignpremcas_14)]) %>%
    add_trace(x=~tranche_cum_doses_2_65_et_plus_debsignpremcas_14, y=~nb_episodes)%>%
    layout(title = "Nombre d'épisodes par tranche vaccinale (2ème injection)",
           xaxis=list(title = "Part de résidents vaccinés dans l'établissement"),
           yaxis=list(title = "Nombre d'épisodes"))
  
  setnames(dt_agg, "tranche_cum_doses_2_65_et_plus_debsignpremcas_14", "Tranche vaccinale")
  setnames(dt_agg, "nb_episodes", "Nombre d'épisodes")
  
  setnames(dt_agg, c("moy_res_conf",	"moy_res_dcd",	"moy_residents", "moy_2_moins14",
                     "moy_1", "moy_1_moins14", "moy_part_vacc"),
           c("Nombre moyen de résidents cas confirmés", "Nombre de résidents décédés",
             "Nombre moyen total de résidents", 
             "Taux de résidents ayant eu leur deuxième dose depuis moins de 14 jours", 
             "Taux ayant eu juste leur première dose mais depuis plus de 14 jours", 
             "Taux ayant eu juste leur première dose depuis moins de 14 jours",
             "Taux partiellement vaccinés"))
  
  return(list(cas = p, episode = p1, tableau = dt_agg))
  
}

gen_graphique_1inj <- function(dte) {
  
  dt_agg <- dte[, .(nb_episodes = .N,
                    moy_part_vacc = mean(taux_partiellement_vacc),
                    moy_2_moins14 = mean(taux_2_doses_moins14),
                    moy_1 = mean(taux_1_dose),
                    moy_1_moins14 = mean(taux_1_dose_moins14),
                    moy_res_conf = mean(nb_conf_res_max),
                    moy_res_dcd = mean(nb_residents_dcd_tot),
                    # moy_perso_conf = mean(nb_conf_perso_max),
                    moy_residents = mean(nb_es_res_total)), by=.(tranche_cum_doses_1_65_et_plus_debsignpremcas_14)]
  
  dt_agg <- dt_agg[order(tranche_cum_doses_1_65_et_plus_debsignpremcas_14)]
  dt_agg[, tranche_cum_doses_1_65_et_plus_debsignpremcas_14 := as.character(tranche_cum_doses_1_65_et_plus_debsignpremcas_14)]
  dt_agg <- mise_en_forme_tranches(dt_agg)
  
  p <- plot_ly(data = dt_agg[order(tranche_cum_doses_1_65_et_plus_debsignpremcas_14)]) %>%
    add_trace(x=~tranche_cum_doses_1_65_et_plus_debsignpremcas_14, y=~moy_res_conf)%>%
    layout(title = "Nombre de résidents cas confirmés par tranche vaccinale (1ère injection)",
           xaxis=list(title = "Part de résidents ayant reçu une première injection"),
           yaxis=list(title = "Nombre moyen de résidents cas confirmés")) %>%
    add_annotations(x = ~tranche_cum_doses_1_65_et_plus_debsignpremcas_14,
                    y = ~moy_res_conf,
                    text = ~paste0(sub("\\.", ",", round(moy_res_conf, 1)) , 
                                   " résidents infectés <br> en moyenne pour les <br>", 
                                   nb_episodes, 
                                   " épisodes signalés"),
                    xref = "x",
                    yref = "y",
                    showarrow = TRUE,
                    # arrowhead = 4,
                    # arrowsize = .5,
                    ax = 0,
                    ay = -30)
  
  p1 <- plot_ly(data = dt_agg[order(tranche_cum_doses_1_65_et_plus_debsignpremcas_14)]) %>%
    add_trace(x=~tranche_cum_doses_1_65_et_plus_debsignpremcas_14, y=~nb_episodes)%>%
    layout(title = "Nombre d'épisodes par tranche vaccinale (1ère injection)",
           xaxis=list(title = "Part de résidents ayant reçu une première injection"),
           yaxis=list(title = "Nombre d'épisodes"))
  
  setnames(dt_agg, "tranche_cum_doses_1_65_et_plus_debsignpremcas_14", "Tranche vaccinale")
  setnames(dt_agg, "nb_episodes", "Nombre d'épisodes")
  
  setnames(dt_agg, c("moy_res_conf",	"moy_res_dcd",	"moy_residents", "moy_2_moins14",
                     "moy_1", "moy_1_moins14", "moy_part_vacc"),
           c("Nombre moyen de résidents cas confirmés", "Nombre de résidents décédés",
             "Nombre moyen total de résidents", 
             "Taux de résidents ayant eu leur deuxième dose depuis moins de 14 jours", 
             "Taux ayant eu juste leur première dose mais depuis plus de 14 jours", 
             "Taux ayant eu juste leur première dose depuis moins de 14 jours",
             "Taux partiellement vaccinés"))
  
  return(list(cas = p, episode = p1, tableau = dt_agg))
  
}


gen_graphique_inj <- function(dte) {
  
  dte[, tranche_injectes_debsignpremcas_14 := cut(taux_vac + taux_partiellement_vacc, c(-0.01, 0, seq(0.25, 1, 0.25), 50), right = TRUE)]
  
  dt_agg <- dte[, .(nb_episodes = .N,
                    moy_part_vacc = mean(taux_partiellement_vacc),
                    moy_2_moins14 = mean(taux_2_doses_moins14),
                    moy_1 = mean(taux_1_dose),
                    moy_1_moins14 = mean(taux_1_dose_moins14),
                    moy_res_conf = mean(nb_conf_res_max),
                    moy_res_dcd = mean(nb_residents_dcd_tot),
                    # moy_perso_conf = mean(nb_conf_perso_max),
                    moy_residents = mean(nb_es_res_total)), by=.(tranche_injectes_debsignpremcas_14)]
  
  dt_agg <- dt_agg[order(tranche_injectes_debsignpremcas_14)]
  dt_agg[, tranche_injectes_debsignpremcas_14 := as.character(tranche_injectes_debsignpremcas_14)]
  dt_agg <- mise_en_forme_tranches(dt_agg)
  
  p <- plot_ly(data = dt_agg[order(tranche_injectes_debsignpremcas_14)]) %>%
    add_trace(x=~tranche_injectes_debsignpremcas_14, y=~moy_res_conf)%>%
    layout(title = "Nombre de résidents cas confirmés par tranche vaccinale (toutes injections)",
           xaxis=list(title = "Part de résidents vaccinés"),
           yaxis=list(title = "Nombre moyen de résidents cas confirmés")) %>%
    add_annotations(x = ~tranche_injectes_debsignpremcas_14,
                    y = ~moy_res_conf,
                    text = ~paste0(sub("\\.", ",", round(moy_res_conf, 1)) , 
                                   " résidents infectés <br> en moyenne pour les <br>", 
                                   nb_episodes, 
                                   " épisodes signalés"),
                    xref = "x",
                    yref = "y",
                    showarrow = TRUE,
                    # arrowhead = 4,
                    # arrowsize = .5,
                    ax = 0,
                    ay = -30)
  
  p1 <- plot_ly(data = dt_agg[order(tranche_injectes_debsignpremcas_14)]) %>%
    add_trace(x=~tranche_injectes_debsignpremcas_14, y=~nb_episodes)%>%
    layout(title = "Nombre d'épisodes par tranche vaccinale (2ème injection)",
           xaxis=list(title = "Part de résidents vaccinés dans l'établissement (ayant reçu deux injections)"),
           yaxis=list(title = "Nombre d'épisodes"))
  
  setnames(dt_agg, "tranche_injectes_debsignpremcas_14", "Tranche vaccinale")
  setnames(dt_agg, "nb_episodes", "Nombre d'épisodes")
  
  setnames(dt_agg, c("moy_res_conf",	"moy_res_dcd",	"moy_residents", "moy_2_moins14",
                     "moy_1", "moy_1_moins14", "moy_part_vacc"),
           c("Nombre moyen de résidents cas confirmés", "Nombre de résidents décédés",
             "Nombre moyen total de résidents", 
             "Taux de résidents ayant eu leur deuxième dose depuis moins de 14 jours", 
             "Taux ayant eu juste leur première dose mais depuis plus de 14 jours", 
             "Taux ayant eu juste leur première dose depuis moins de 14 jours",
             "Taux partiellement vaccinés"))
  
  return(list(cas = p, episode = p1, tableau = dt_agg))
  
}

mise_en_forme_tranches <- function(dt_res_){
  
  dt_res_[dt_res_ == "(-0.01,0]"] <- "Aucune injection"
  dt_res_[dt_res_ == "(0,0.2]"] <- "Entre 0 % et 20 %"
  dt_res_[dt_res_ == "(0.2,0.4]"] <- "Entre 20 % et 40 %"
  dt_res_[dt_res_ == "(0.4,0.6]"] <- "Entre 40 % et 60 %"
  dt_res_[dt_res_ == "(0.6,0.8]"] <- "Entre 60 % et 80 %"
  dt_res_[dt_res_ == "(0.8,1]"] <- "Entre 80 % et 100 %"
  dt_res_[dt_res_ == "(1,50]"] <- "Strictement plus de 100 %"
  
  
  dt_res_[dt_res_ == "(0,0.5]"] <- "Entre 0 % et 50 %"
  dt_res_[dt_res_ == "(0.5,1]"] <- "Entre 50 % et 100 %"
  
  dt_res_[dt_res_ == "(0,0.25]"] <- "Entre 0 % et 25 %"
  dt_res_[dt_res_ == "(0.25,0.5]"] <- "Entre 25 % et 50 %"
  dt_res_[dt_res_ == "(0.5,0.75]"] <- "Entre 50 % et 75 %"
  dt_res_[dt_res_ == "(0.75,1]"] <- "Entre 75 % et 100 %"
  
  return(dt_res_)
  
}
