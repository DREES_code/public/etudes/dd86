# La vaccination contre la Covid-19 en Ehpad et son effet sur la contamination des résidents

Ce dossier fournit les programmes permettant de produire les chiffres et les illustrations contenus dans le texte du Dossier de la DREES "La vaccination contre la Covid-19 en Ehpad et son effet sur la contamination des résidents", novembre 2021. Il mobilise des données agrégées du système d'information sur la vaccination (Vac-SI, CNAM) et les données brutes de l'enquête Covid-ESMS de Santé Publique France, à l'échelle des établissements. 

Les codes permettent de rapprocher le nombre d'injections de vaccin contre la Covid-19 faites à destination de résidents d'Ehpad au nombre de contaminations confirmées par test déclarées au sein de cette population. Le croisement est réalisé grâce au numéro Finess des Ehpad. Nous mettons ensuite en œuvre une modélisation pour identifier l'effet de la couverture vaccinale sur le nombre de contaminations, tout en neutralisant de potentiels effets de structure des établissements.

**Mode d'emploi du code :**

Script 0 : prétraitement des données de Vac-SI
Scripts 1, 2, 4, 5, 6 : génération des graphiques de statistique descriptive.
Scripts 3 : traitement des taux de vaccination et calcul des couvertures vaccinales au début de chaque épisode de Covid en Ehpad en 2021.
Script 7 : modélisation de l'effet de la vaccination. 


Lien vers l'étude : https://drees.solidarites-sante.gouv.fr/etudes-et-statistiques/publications/...

Présentation de la DREES : La Direction de la recherche, des études, de l'évaluation et des statistiques (DREES) est le service statistique ministériel des ministères sanitaires et sociaux, et une direction de l'administration centrale de ces ministères.
https://drees.solidarites-sante.gouv.fr/article/presentation-de-la-drees

Source de données : Vac-SI (CNAM), extraction au 16/06/2021. Enquête Covid-ESMS (SPF) versions 1 et 2, extraction au 30/06/2021. Référentiel Finess, extraction au 30/01/2021. Traitements : Drees.
Date de la dernière exécution des programmes avant publication, et version des logiciels utilisés : Les programmes ont été exécutés pour la dernière fois avec le logiciel R version 3.6.1., le 09/11/2021.
