---
title: "Modèles pour le DD vaccination en Ehpad"
author: "Albane Miron de l'Espinay"
output: html_document
---

Copyright (C) 2021. Logiciel élaboré par l'État, via la Drees.

Nom de l'auteur : Albane Miron de l'Espinay, Drees.

Ce programme informatique a été développé par la Drees. Il permet de produire les chiffres publiés dans le Dossier de la Drees "La vaccination contre la Covid-19 en Ehpad et son effet sur la contamination des résidents", novembre 2021.

Ce programme a été exécuté le 09/11/2021 avec la version 3.6.1 de R et les packages  margins v0.3.26    stringr v1.4.0     MASS v7.3-51.4     pscl v1.5.5        plotly v4.9.2.1    ggplot2 v3.3.3     AER v1.2-9         survival v2.44-1.1 sandwich v2.5-1    lmtest v0.9-37     zoo v1.8-6         car v3.0-4         carData v3.0-2  data.table v1.13.0 knitr v1.25   

Le texte et les tableaux de l'article peuvent être consultés sur le site de la DREES : XXXXXXX

Ce programme mobilise des données agrégées du système d'information sur la vaccination (Vac-SI, CNAM) et les données brutes de l'enquête Covid-ESMS de Santé Publique France, à l'échelle des établissements.

Bien qu'il n'existe aucune obligation légale à ce sujet, les utilisateurs de ce programme sont invités à signaler à la DREES leurs travaux issus de la réutilisation de ce code, ainsi que les éventuels problèmes ou anomalies qu'ils y rencontreraient, en écrivant à DREES-CODE@sante.gouv.fr

Ce logiciel est régi par la licence "GNU General Public License" GPL-3.0. https://spdx.org/licenses/GPL-3.0.html#licenseText

À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence GPL-3.0, et que vous en avez accepté les termes.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.If not, see https://www.gnu.org/licenses/.

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning = FALSE, message = FALSE, fig.width=9, fig.height=6)
options(scipen=999, width=100)
```

```{r}
njepisode <- 14 # situation de l'épisode au bout du nème jour après le début des signes du premier cas 
mois_ciblage <- "nov2020" # mars2021 nov2020
version_voo <- 1 # ou 1
```

Paramètres : 
Episodes de la version `r version_voo` de Covid-Esms. 
Ciblage des résidents en Ehpad qui date de `r mois_ciblage`.
Situation des épisodes à `r njepisode` jours après le début des signes du premier cas. 

```{r, echo=FALSE}
library(knitr)
library(data.table)
library(AER)
library(plotly)
library(pscl)
library(MASS)
library(stringr)
library(margins)

# source("utils.R", encoding = "UTF-8")
source("scripts/1-episodes-en-cours.R", encoding = "UTF-8")
source("scripts/2-indicateurs-lien-vacc-episode.R", encoding = "UTF-8")

sub_file <- "_v2"
if(version_voo == 1){
  sub_file <- ""
}

fichier_resultats <- "<chemin vers le Excel de résultats>"
chemin_donnees_vacc <- "<chemin vers les données de VACSI de la CNAM>" # donnees de vacsi
chemin_appariem_ehpa <- "<chemin vers les données provisoires d'EHPA 2019>"

# ajout d'un filtre sur les Ehpad selectionnables : généré dans 6
dt_selec <- fread(paste0(chemin_donnees_vacc, "ciblage_ehpad_",mois_ciblage,".csv")) # dans script 6
dt <- fread(paste0(chemin_donnees_vacc, "episodes_a_",njepisode,"_jours",sub_file,"_", mois_ciblage ,".csv")) # générés dans le script 4
dt[, finess := str_pad(finess, 9,  pad="0")] # 1145 épisodes en Ehpad

# uniquement établissements de ResidEhpad validés et ajout du nb de résidents ciblés
dt <- merge(dt, dt_selec[, c("nofiness", "nb")], by.x="finess", by.y = 'nofiness') # 757 concernent des Ehpad suffisamment bien ciblés dans ResidEhpad

dtehpa0 <- fread(chemin_appariem_ehpa)
# colonnes EHPA dans l'ordre décroissant de remplissage
dtehpa <- dtehpa0[, c("FINESSETAB", "GMPVAL", "PMP", "VEILLEINF", "DIFRECRUT", "TELEMED", "txEncadr" ,"sans_sanitaire",  "annee_bati", "GMP_tranche", "PMP_tranche")]

dt <- merge(dt, dtehpa, by.x="finess", by.y="FINESSETAB", all.x = TRUE)
dt[, scaled.PMP := scale(PMP)]
dt[, scaled.GMPVAL := scale(GMPVAL)]
```

```{r}
dt <- fonction_netoyage(dt, restriction_episodes_confirmes = TRUE, excl_pub_hosp = FALSE, excl_plus_100 = TRUE)
if(version_voo == 1) {
  dt <- dt[semaine %in% c("6","7","8") & finess != "770814952"]
}
```

# Statistiques descriptives 

## Nombre d'épisodes par tranche vaccinale 

Nombre d'épisodes pour lesquels on a leur situation à `r njepisode` jours après le début des signes du premier cas. 

En seconde dose plus de 14 jours avant le début des signes du premier cas. 
```{r}
gr <- gen_graphique(dt)
kable(gr$tableau)
```

En première dose plus de 14 jours avant le début des signes du premier cas. 

```{r}
gr_inj <- gen_graphique_inj(dt)
kable(gr_inj$tableau)
```

## Correlation entre les variables de contrôle

Chi-squared test.

```{r}

tbl <- dt[region != "REU" & region != "GUA", .(Effectif = .N), by=.(region, statut_jur_lib)]
tbl <- dcast(tbl, region~statut_jur_lib, value.var="Effectif")
tbl[, Total := `Privé commercial`+`Privé non lucratif`+`Public hospitalier`+`Public non hospitalier`]
# tbl[, `:=`(priv_com = 100*`Privé commercial`/Total,
#            priv_non_luc = 100 * `Privé non lucratif` /Total)]

row.names(tbl) <- tbl$region
tbl[, region := NULL]

chi2 = chisq.test(tbl, simulate.p.value = TRUE)
c(chi2$statistic, chi2$p.value)
```
Ici, on peut rejeter l'hypothèse d'indépendance. 

```{r}
aov1 = aov(nb_es_res_total ~ statut_jur_lib, data=dt)
summary(aov1)
```
```{r}
aov1 = aov(PMP ~ statut_jur_lib, data=dt)
summary(aov1)
```
## Correlation entre les taux de vaccination et des variables de contrôle

```{r}
aov1 = aov(taux_vac ~ statut_jur_lib, data=dt)
summary(aov1)
```

Pas de correlation avec le statut juridique. 


```{r}
aov1 = aov(taux_vac ~ region, data=dt)
summary(aov1)
```
Ni avec la région. 

## Correlation entre les taux de vaccination

`r cor(dt$taux_vac, dt$taux_partiellement_vacc)` pour le taux de secondes injections depuis plus de 14 jours et du taux de partiellement vaccinés. 

Pour les taux de secondes inj depuis plus de 14 jours avec taux depuis moins de 14 jours, taux de première inj depuis plus de 14 jours, taux de première inj depuis moins de 14 jours. 
```{r}
cor(dt$taux_vac, dt$taux_2_doses_moins14)
cor(dt$taux_vac, dt$taux_1_dose)
cor(dt$taux_vac, dt$taux_1_dose_moins14)
```


Au mois d'avril les couvertures vaccinales première et seconde dose sont trop colinéaires pour être incluses dans le modèle. Pour le mois d'avril on ne met que le taux de résidents vaccinés seconde dose depuis plus de 14 jours. 

Au mois de février la colinéarité est suffisamment basse, à part avec le nombre de secondes injections réalisées il y a moins de 14 jours. Il faut probablement faire une variable "une dose depuis plus de 14 jours". 

Autre essai : prendre la somme des deux taux de vaccination, car il y a pu avoir des résidents totalement vaccinés dès la première dose, s'ils avaient déjà été contaminés par le virus. Pas le cas pour les épisodes de mars, car ce n'était pas encore les consignes appliquées. 

### En première injection depuis plus de 14 jours
```{r}
library(rdrees)

gr1 <- gen_graphique_1inj(dt)

exporter_tableau_BPC(gr1$tableau, 
                     onglet="Graphique 5.A",
                     fichier_source = fichier_resultats, 
                     note = "Nombre cumulé de cas confirmés parmi les résidents au 14ème jour après le début des signes du premier cas de l’épisode.",
                     lecture = "14 jours après le début de l’épisode dans l’établissement, 0,6 résident ont été testé positifs à la Covid-19 en moyenne parmi les 15 épisodes d’infection signalés dans des Ehpad où plus de 75 % des résidents avaient reçu leur seconde injection de vaccin plus de 14 jours avant le début des signes du premier cas (observé parmi le résident ou personnel).",
                     titre = "Nombre moyen de résidents cas confirmés lors d’un épisode de Covid-19 au sein d’un Ehpad, par taux de couverture vaccinale des résidents en première dose",
                     champ = "Ehpad de France métropolitaine rattachés au régime général ayant déclaré un épisode de Covid-19 qui a débuté entre le 8 et le 27 février 2021 et qui a duré plus de 14 jours au 13 mars 2021.",
                     source = "VacSi (CNAM), ResidEhpad (CNAM) et Covid-ESMS v1 (SPF), traitements DREES.", 
                     visualiser = FALSE, 
                     charte_graphique = "social"
)

```


```{r}
gr1$cas
```


### En seconde injection depuis plus de 14 jours 
```{r}
gr$cas
```

```{r}
exporter_tableau_BPC(gr$tableau, 
                     onglet="Graphique 5.B",
                     fichier_source = fichier_resultats, 
                     note = "Nombre cumulé de cas confirmés parmi les résidents au 14ème jour après le début des signes du premier cas de l’épisode.",
                     lecture = "14 jours après le début de l’épisode dans l’établissement, 0,6 résident ont été testé positifs à la Covid-19 en moyenne parmi les 15 épisodes d’infection signalés dans des Ehpad où plus de 75 % des résidents avaient reçu leur seconde injection de vaccin plus de 14 jours avant le début des signes du premier cas (observé parmi le résident ou personnel).",
                     titre = "Nombre moyen de résidents cas confirmés lors d’un épisode de Covid-19 au sein d’un Ehpad, par taux de couverture vaccinale des résidents en seconde dose",
                     champ = "Ehpad de France métropolitaine rattachés au régime général ayant déclaré un épisode de Covid-19 qui a débuté entre le 8 et le 27 février 2021 et qui a duré plus de 14 jours au 13 mars 2021.",
                     source = "VacSi (CNAM), ResidEhpad (CNAM) et Covid-ESMS v1 (SPF), traitements DREES.", 
                     visualiser = FALSE, 
                     charte_graphique = "social"
)
```


### En nombre total d'injections
```{r}
gr_inj$cas
```

## Nombre de résidents cas confirmés par épisode

```{r}
summary(dt$nb_conf_res_max)
```

## Nombre de résidents décédés par épisode

```{r}
summary(dt$nb_residents_dcd_tot)
```


# Modélisation 

## Nombre de résidents cas confirmés

### Avec la déclinaison des taux de vaccination

On propose un modèle de Poisson, et un modèle Zero-Inflated-Poisson pour traiter le problème de la masse importante de 0. Des tests de spécification de ces modèles montrent que les meilleures covariables disponibles à inclure en plus du taux de secondes injections sont le nombre de résidents total, la semaine, la région et le statut de l'établissement. 

Modèle de Poisson
```{r}
dt[, region := as.factor(region)]
dt[, region := relevel(region, ref="ARA")]

reg1_3 <- glm(nb_conf_res_max ~ taux_vac + taux_2_doses_moins14+
                taux_1_dose + taux_1_dose_moins14 
                + nb_es_res_total + sqrt_nb_es_res_total + semaine + 
                region + statut_jur_lib 
              + scaled.GMPVAL + scaled.PMP, 
              data = dt[region != "REU" & region != "GUA"], 
              family = poisson())
summary(reg1_3)

# res1 <- summary(margins(reg1_3, type = "link", data=dt[region != "REU" & region != "GUA"])) # the default : effect on the outcome scale
res1 <- data.frame(coefficient = exp(coef(reg1_3)))
fwrite(res1, paste0("data/regression_v",version_voo,"_",njepisode,"jours_poisson.csv"), row.names = TRUE)

# Boostrap
# install.packages('boot')
library(boot)
indices <- c(1:nrow(dt[region != "REU" & region != "GUA"])) # numérotation des individus
myFunc <- function(data, indices, formula) {data <- data[indices, ]; fit <- glm(formula, data, 
              family = poisson()); return((coef(fit)))}
bt <- boot(data  = dt[region != "REU" & region != "GUA"], statistic = myFunc, R = 500, formula = nb_conf_res_max ~ taux_vac + taux_2_doses_moins14+
                taux_1_dose + taux_1_dose_moins14 
                + nb_es_res_total + sqrt_nb_es_res_total + semaine + 
                region + statut_jur_lib + scaled.GMPVAL + scaled.PMP)
bt$t
bt$t0[2]
summary(exp((bt$t[,2])*0.1)-1)
var(bt$t[,2])
sd(bt$t[,2])
exp((bt$t0[2])*0.1)-1
exp(mean(bt$t[,2])*0.1)-1
exp(max(bt$t[,2])*0.1)-1
exp(min(bt$t[,2])*0.1)-1
exp(median(bt$t[,2])*0.1)-1
exp(sd(bt$t[,2])*0.1)-1
exp(var(bt$t[,2])*0.1)-1

```


```{r}

res_reg <- summary(reg1_3)
res_reg <- data.frame(res_reg$coefficients)
res_reg$Variable <- row.names(res_reg)
res_reg <- data.table(res_reg)
res_reg[, Estimate.exp.1 := exp(Estimate)-1]
res_reg[, Estimate.0.1 := Estimate*0.1]
res_reg[, std.error.0.1 := `Std..Error`*0.1]

res_reg[, exp.Estimate.0.1 := exp(Estimate.0.1) - 1]
res_reg <- res_reg[Variable %in% c("taux_vac", "taux_2_doses_moins14",
                                   "taux_1_dose", "taux_1_dose_moins14")]
res_reg[res_reg=="taux_vac"] <- "Seconde dose depuis <br> plus de 14 jours"
res_reg[res_reg=="taux_2_doses_moins14"] <- "Seconde dose depuis <br> moins de 14 jours"
res_reg[res_reg=="taux_1_dose"] <- "Première dose depuis <br> plus de 14 jours"
res_reg[res_reg=="taux_1_dose_moins14"] <- "Première dose depuis <br> moins de 14 jours*"

m <- res_reg[Variable=="Seconde dose depuis plus de 14 jours"]

a <- list(
  x = m$Variable,
  y = round(m$exp.Estimate.0.1*100, 1),
  text = paste0("Une augmentation de 10 points de la CV des résidents de l'Ehpad <br> est associée à une réduction de ", paste0(round(m$exp.Estimate.0.1*100, 1), "%"), "<br>  du nombre moyen de résidents cas confirmés <br> en cas d'épisode de Covid-19 dans l'établissement <br> toutes choses égales par ailleurs"),
  xref = "x",
  yref = "y",
  showarrow = TRUE,
  arrowhead = 1,
  ax = 200,
  ay = -40
)


peff <- plot_ly(data = res_reg)%>%
  add_trace(x=~Variable, y=~round(exp.Estimate.0.1*100, 1),type="bar")%>%# type="scatter", mode="lines")%>%
  add_annotations(x=~Variable, y=~round(exp.Estimate.0.1*100, 1),
                  text=~paste0(sub("\\.", ",", round(exp.Estimate.0.1*100, 1)), " %"), xref="x", yref="y", showarrow = TRUE,
                  ax = 0,
                  ay = 20)%>%
  layout(xaxis=list(title="",
                    zeroline = FALSE,
                    showline = FALSE,
                    showticklabels = TRUE,
                    showgrid = FALSE), yaxis=list(title="Variation de la moyenne du nombre de résidents infectés",
                                                  zeroline = TRUE,
                                                  showline = FALSE,
                                                  showticklabels = TRUE,
                                                  showgrid = FALSE)
         #,
         # annotations=a
         )

peff
```

ZIP 

```{r}
reg3_zi <- zeroinfl(nb_conf_res_max~ taux_vac + taux_2_doses_moins14+
                      taux_1_dose + taux_1_dose_moins14 +
                      + nb_es_res_total + sqrt_nb_es_res_total + semaine + region + statut_jur_lib + scaled.GMPVAL + scaled.PMP, 
                    data = dt[region != "REU" & region != "GUA"], dist = "poisson",link="logit")

summary(reg3_zi)

```

```{r}
res_zip <- summary(reg3_zi)
res_zip <- data.frame(res_zip$coefficients$count)

res_zip$Variable <- row.names(res_zip)
res_zip <- data.table(res_zip)
res_zip[, Estimate.0.1 := Estimate*0.1]
res_zip[, std.error.0.1 := `Std..Error`*0.1]

res_zip[, exp.Estimate.0.1 := exp(Estimate.0.1) - 1]
res_zip[, exp.std.error.0.1 := exp(std.error.0.1) - 1]
res_zip <- res_zip[Variable %in% c("taux_vac", "taux_2_doses_moins14",
                                   "taux_1_dose", "taux_1_dose_moins14")]
res_zip[res_zip=="taux_vac"] <- "CV seconde dose depuis plus de 14 jours"
res_zip[res_zip=="taux_2_doses_moins14"] <- "CV seconde dose depuis moins de 14 jours"
res_zip[res_zip=="taux_1_dose"] <- "CV première dose depuis plus de 14 jours"
res_zip[res_zip=="taux_1_dose_moins14"] <- "CV première dose depuis moins de 14 jours*"

m <- res_zip[Variable=="CV seconde dose depuis plus de 14 jours"]

a <- list(
  x = m$Variable,
  y = round(m$exp.Estimate.0.1*100, 1),
  text = paste0("Une augmentation de 10 points de la CV des résidents de l'Ehpad <br> est associée à une réduction de ", paste0(round(m$exp.Estimate.0.1*100, 1), "%"), "<br>  du nombre moyen de résidents cas confirmés <br> en cas d'épisode de Covid-19 dans l'établissement <br> toutes choses égales par ailleurs"),
  xref = "x",
  yref = "y",
  showarrow = TRUE,
  arrowhead = 1,
  ax = 200,
  ay = -40
)


peff <- plot_ly(data = res_zip)%>%
  add_trace(x=~Variable, y=~round(exp.Estimate.0.1*100, 1), type="scatter", mode="lines")%>%
  add_annotations(x=~Variable, y=~round(exp.Estimate.0.1*100, 1),
                  text=~paste0(round(exp.Estimate.0.1*100, 1), "%"), xref="x", yref="y", showarrow = TRUE,
                  ax = 0,
                  ay = 20)%>%
  layout(xaxis=list(title="",
                    zeroline = FALSE,
                    showline = FALSE,
                    showticklabels = TRUE,
                    showgrid = FALSE), yaxis=list(title="Variation de la moyenne du nombre de résidents infectés",
                                                  zeroline = TRUE,
                                                  showline = FALSE,
                                                  showticklabels = TRUE,
                                                  showgrid = FALSE)
         # ,
         # annotations=a
         )

peff

```

### Avec uniquement le taux total de résidents ayant reçu au moins une injection depuis plus de 14 jours 

Modèle de Poisson
```{r}

reg1_3 <- glm(nb_conf_res_max ~ taux_injectes
                + nb_es_res_total + sqrt_nb_es_res_total + semaine + 
                region + statut_jur_lib + scaled.GMPVAL + scaled.PMP, 
              data = dt[region != "REU" & region != "GUA"], 
              family = poisson())
summary(reg1_3)

```


```{r}

res_reg <- summary(reg1_3)
res_reg <- data.frame(res_reg$coefficients)
res_reg$Variable <- row.names(res_reg)
res_reg <- data.table(res_reg)
res_reg[, Estimate.0.1 := Estimate*0.1]
res_reg[, std.error.0.1 := `Std..Error`*0.1]

res_reg[, exp.Estimate.0.1 := exp(Estimate.0.1) - 1]
res_reg <- res_reg[Variable %in% c("taux_injectes")]
res_reg[res_reg=="taux_injectes"] <- "CV une injection au moins depuis plus de 14 jours"

m <- res_reg[Variable=="CV une injection au moins depuis plus de 14 jours"]

a <- list(
  x = m$Variable,
  y = round(m$exp.Estimate.0.1*100, 1),
  text = paste0("Une augmentation de 10 points de la CV des résidents de l'Ehpad <br> est associée à une réduction de ", paste0(round(m$exp.Estimate.0.1*100, 1), "%"), "<br>  du nombre moyen de résidents cas confirmés <br> en cas d'épisode de Covid-19 dans l'établissement <br> toutes choses égales par ailleurs"),
  xref = "x",
  yref = "y",
  showarrow = TRUE,
  arrowhead = 1,
  ax = 200,
  ay = -40
)


peff <- plot_ly(data = res_reg)%>%
  add_trace(x=~Variable, y=~round(exp.Estimate.0.1*100, 1), type="scatter", mode="lines")%>%
  add_annotations(x=~Variable, y=~round(exp.Estimate.0.1*100, 1),
                  text=~paste0(round(exp.Estimate.0.1*100, 1), "%"), xref="x", yref="y", showarrow = TRUE,
                  ax = 0,
                  ay = 20)%>%
  layout(xaxis=list(title="",
                    zeroline = FALSE,
                    showline = FALSE,
                    showticklabels = TRUE,
                    showgrid = FALSE), yaxis=list(title="Variation de la moyenne du nombre de résidents infectés",
                                                  zeroline = TRUE,
                                                  showline = FALSE,
                                                  showticklabels = TRUE,
                                                  showgrid = FALSE),
         annotations=a)

peff
```

ZIP 

```{r}
reg3_zi <- zeroinfl(nb_conf_res_max~ taux_injectes
                      + nb_es_res_total + sqrt_nb_es_res_total + semaine + region + statut_jur_lib + scaled.GMPVAL + scaled.PMP, 
                    data = dt[region != "REU" & region != "GUA"], dist = "poisson",link="logit")

summary(reg3_zi)

```

```{r}
res_zip <- summary(reg3_zi)
res_zip <- data.frame(res_zip$coefficients$count)

res_zip$Variable <- row.names(res_zip)
res_zip <- data.table(res_zip)
res_zip[, Estimate.0.1 := Estimate*0.1]
res_zip[, std.error.0.1 := `Std..Error`*0.1]

res_zip[, exp.Estimate.0.1 := exp(Estimate.0.1) - 1]
res_zip[, exp.std.error.0.1 := exp(std.error.0.1) - 1]
res_zip <- res_zip[Variable %in% c("taux_injectes")]
res_zip[res_zip=="taux_injectes"] <- "CV une injection au moins depuis plus de 14 jours"

m <- res_zip[Variable=="CV une injection au moins depuis plus de 14 jours"]

a <- list(
  x = m$Variable,
  y = round(m$exp.Estimate.0.1*100, 1),
  text = paste0("Une augmentation de 10 points de la CV des résidents de l'Ehpad <br> est associée à une réduction de ", paste0(round(m$exp.Estimate.0.1*100, 1), "%"), "<br>  du nombre moyen de résidents cas confirmés <br> en cas d'épisode de Covid-19 dans l'établissement <br> toutes choses égales par ailleurs"),
  xref = "x",
  yref = "y",
  showarrow = TRUE,
  arrowhead = 1,
  ax = 200,
  ay = -40
)


peff <- plot_ly(data = res_zip)%>%
  add_trace(x=~Variable, y=~round(exp.Estimate.0.1*100, 1), type="scatter", mode="lines")%>%
  add_annotations(x=~Variable, y=~round(exp.Estimate.0.1*100, 1),
                  text=~paste0(round(exp.Estimate.0.1*100, 1), "%"), xref="x", yref="y", showarrow = TRUE,
                  ax = 0,
                  ay = 20)%>%
  layout(xaxis=list(title="",
                    zeroline = FALSE,
                    showline = FALSE,
                    showticklabels = TRUE,
                    showgrid = FALSE), yaxis=list(title="Variation de la moyenne du nombre de résidents infectés",
                                                  zeroline = TRUE,
                                                  showline = FALSE,
                                                  showticklabels = TRUE,
                                                  showgrid = FALSE),
         annotations=a)

peff

```

Dans la partie logarithmique du ZIP, on ne peut rejeter l'hypothèse de nullité du coefficient pour aucune variable, et pas même l'intercept. Cela argumente pour retenir plutôt le modèle de Poisson, dont la forme fonctionnelle semble suffisante. 


## Nombre de résidents décédés du virus 

### Prédiction de l'indicatrice au moins un décès

Etant limités par le manque de profondeur des données, nous avons choisi d'étudier le fait d'avoir au moins un décès durant l'épisode plutôt que le nombre de décès parmi les résidents, à partir d'une régression logistique (avec les mêmes covariables que pour le nombre de résidents infectés): 

Avec la déclinaison des taux de vaccination

```{r}
reg3_1 <- glm(indic_dcd ~ taux_vac + taux_2_doses_moins14+
                      taux_1_dose + taux_1_dose_moins14 +
                      + nb_es_res_total + sqrt_nb_es_res_total + semaine + region + statut_jur_lib + scaled.GMPVAL + scaled.PMP , 
              data = dt[region!="REU"], 
              family = binomial(link = "logit"))
summary(reg3_1)
```

Avec un taux de résidents ayant reçu une injection 

```{r}
reg3_1 <- glm(indic_dcd ~ taux_injectes
                      + nb_es_res_total + sqrt_nb_es_res_total + semaine + region + statut_jur_lib + scaled.GMPVAL + scaled.PMP , 
              data = dt[region!="REU"], 
              family = binomial(link = "logit"))
summary(reg3_1)
```

Aucune possibilité de rejeter l'hypothèse nulle. 

### Prédiction du nombre de décès dans le cas où un résident est touché

Modèle de Poisson.
```{r}

reg1_3 <- glm(nb_residents_dcd_tot ~ taux_injectes
                + nb_es_res_total + sqrt_nb_es_res_total + semaine + 
                region + statut_jur_lib + scaled.GMPVAL + scaled.PMP, 
              data = dt[region != "REU" & region != "GUA"], 
              family = poisson())
summary(reg1_3)

```


```{r}

res_reg <- summary(reg1_3)
res_reg <- data.frame(res_reg$coefficients)
res_reg$Variable <- row.names(res_reg)
res_reg <- data.table(res_reg)
res_reg[, Estimate.0.1 := Estimate*0.1]
res_reg[, std.error.0.1 := `Std..Error`*0.1]

res_reg[, exp.Estimate.0.1 := exp(Estimate.0.1) - 1]
res_reg <- res_reg[Variable %in% c("taux_injectes")]
res_reg[res_reg=="taux_injectes"] <- "CV une injection au moins depuis plus de 14 jours"

m <- res_reg[Variable=="CV une injection au moins depuis plus de 14 jours"]

a <- list(
  x = m$Variable,
  y = round(m$exp.Estimate.0.1*100, 1),
  text = paste0("Une augmentation de 10 points de la CV des résidents de l'Ehpad <br> est associée à une réduction de ", paste0(round(m$exp.Estimate.0.1*100, 1), "%"), "<br>  du nombre moyen de résidents cas confirmés <br> en cas d'épisode de Covid-19 dans l'établissement <br> toutes choses égales par ailleurs"),
  xref = "x",
  yref = "y",
  showarrow = TRUE,
  arrowhead = 1,
  ax = 200,
  ay = -40
)


peff <- plot_ly(data = res_reg)%>%
  add_trace(x=~Variable, y=~round(exp.Estimate.0.1*100, 1), type="scatter", mode="lines")%>%
  add_annotations(x=~Variable, y=~round(exp.Estimate.0.1*100, 1),
                  text=~paste0(round(exp.Estimate.0.1*100, 1), "%"), xref="x", yref="y", showarrow = TRUE,
                  ax = 0,
                  ay = 20)%>%
  layout(xaxis=list(title="",
                    zeroline = FALSE,
                    showline = FALSE,
                    showticklabels = TRUE,
                    showgrid = FALSE), yaxis=list(title="Variation de la moyenne du nombre de résidents infectés",
                                                  zeroline = TRUE,
                                                  showline = FALSE,
                                                  showticklabels = TRUE,
                                                  showgrid = FALSE),
         annotations=a)

peff
```

## Calcul de l'effet marginal du nombre de résidents dans l'établissement 

Pour la régression présentée dans la publication. 

Moyenne du nombre de résidents par établissement : 84.

attr(,"scaled:center")
[1] 7084.89
attr(,"scaled:scale")
[1] 11838.37

Coef de Poisson : 
nb_es_res_total                       0.008121   0.001093   7.427    0.000000000000111 ***
sqrt_nb_es_res_total                 -0.089172   0.027513  -3.241             0.001191 ** 

On fait : 
$$effet = exp(\beta_{lin}*1 + \beta_{sq}*\Delta X^2_{renorm})$$

```{r}
delta_sq <- (85*85 - 84*84)/11838.37
coef_sq <- -0.089172
coef_lin <- 0.008121

# d'où effet : 
effet <- (exp(coef_lin)- 1)*100
effet_sq <- (exp(coef_sq*delta_sq) -1)*100

```

