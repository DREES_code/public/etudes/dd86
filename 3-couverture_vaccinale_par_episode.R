# Script qui calcule la couverture vaccinale des résidents d'un Ehpad au début de l'épisode infectieux

#  Copyright (C) 2021. Logiciel élaboré par l'État, via la Drees.

#  Nom de l'auteur : Albane Miron de l'Espinay, Drees.

#  Ce programme informatique a été développé par la Drees. Il permet de produire les tableaux et graphiques publiés dans le Dossier de la Drees "La vaccination contre la Covid-19 en Ehpad et son effet sur la contamination des résidents", novembre 2021.

# Ce programme a été exécuté le 09/11/2021 avec la version 3.6.1 de R et le package data.table V.1.13.0

# Le texte et les tableaux de l'article peuvent être consultés sur le site de la DREES https://drees.solidarites-sante.gouv.fr/publications/les-dossiers-de-la-drees/la-vaccination-contre-la-covid-19-en-ehpad-et-son-effet-sur

#  Ce programme mobilise des données agrégées du système d'information sur la vaccination (Vac-SI, CNAM) et les données brutes de l'enquête Covid-ESMS de Santé Publique France, à l'échelle des établissements.

#  Bien qu'il n'existe aucune obligation légale à ce sujet, les utilisateurs de ce programme sont invités à signaler à la DREES leurs travaux issus de la réutilisation de ce code, ainsi que les éventuels problèmes ou anomalies qu'ils y rencontreraient, en écrivant à DREES-CODE@sante.gouv.fr


# Ce logiciel est régi par la licence "GNU General Public License" GPL-3.0. # https://spdx.org/licenses/GPL-3.0.html#licenseText

# À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

# Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence GPL-3.0, et que vous en avez accepté les termes.



# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.If not, see <https://www.gnu.org/licenses/>.


library(data.table)
njrecul <- 14
date_situation <- "2021-03-13" # max avec les données de la v1 : 13 mars 2021
date_deb_vaccination <- "2021-01-15"

mois_ciblage <- "mars2021" # ou "mars2021" nov2020

fichier_resultats <- "<chemin vers le Excel de résultats>"
chemin_donnees_vacc <- "<chemin vers les données de VACSI de la CNAM>" # donnees de vacsi
chemin_data_v1 <- "<chemin vers les données de la v1 de l'enquete Covid-ESMS de SPF>" # données voozanoo v1
chemin_data_v2 <- "<chemin vers les données de la v2 de l'enquete Covid-ESMS de SPF>"# données voozanoo v2

source("scripts/1-episodes-en-cours.R", encoding = "UTF-8")

# historique des vaccinations en Ehpad
dtvac_hist <- fread(paste0(chemin_donnees_vacc, "injections_par_ehpad_ciblage_",mois_ciblage ,".csv"))#"data/historique_vaccination.csv") # généré dans 1-exploratoire-vac-si.R ou taux_vaccination_vacsi_ciblage.R
dtvac_hist[, `:=`(nb_doses_1_moins_de_65 = 0, nb_doses_2_moins_de_65 = 0)]
dtvac_hist2 <- ajout_cumul(dtvac_hist)

# référentiel Finess
dt_finess <- fread("data/finess_full_ehpad_janv21.csv")
dt_finess[, tot_cap := round(pmin(tot_capaut_finess, tot_capinst_finess, na.rm = T))]
dt_finess[is.na(tot_cap), tot_cap := median(dt_finess$tot_cap, na.rm=T)]
dt_finess[, cap_groupe := cut(tot_cap, c(0, 50, 100, 800), right = FALSE)]

# episodes en Ehpad

dt_hist <- fread(paste0(chemin_data_v1, "base_maximale_historique.csv"))
dt_hist[, date_deb_sign_premcasc_ep := as.Date(date_deb_sign_premcasc_ep)]
dt_hist[, derniere_date := as.Date(derniere_date)]
dt_hist <- fill_col_total_deces(dt_hist)

# épisodes à n jours
dt_hist_e <- dt_hist[sous_categorie_esms=="EHPAD"]

dt_test21 <- situation_episode_n_jours(dt_hist_e, 21, date_deb_vaccination, date_situation)
dt_21 <- ajout_situation_vaccinale_episode(dt_test21, dtvac_hist2, njrecul_ = njrecul) 

dt_21 <- merge(dt_21, dt_finess, by.x="finess", by.y="nofiness", all.x=T)
dt_21[, nb_total_residents := tot_cap]

fwrite(dt_21, paste0(chemin_donnees_vacc, "episodes_a_21_jours_",mois_ciblage ,".csv"))

dt_test14 <- situation_episode_n_jours(dt_hist_e, 14, date_deb_vaccination, date_situation)
dt_14 <- ajout_situation_vaccinale_episode(dt_test14, dtvac_hist2, njrecul_ = njrecul) 

dt_14 <- merge(dt_14, dt_finess, by.x="finess", by.y="nofiness", all.x=T)
dt_14[, nb_total_residents := tot_cap]

fwrite(dt_14, paste0(chemin_donnees_vacc, "episodes_a_14_jours_",mois_ciblage ,".csv"))

dt_test7 <- situation_episode_n_jours(dt_hist_e, 7, date_deb_vaccination, date_situation)
dt_7 <- ajout_situation_vaccinale_episode(dt_test7, dtvac_hist2, njrecul_ = njrecul) 

dt_7 <- merge(dt_7, dt_finess, by.x="finess", by.y="nofiness", all.x=T)
dt_7[, nb_total_residents := tot_cap]

fwrite(dt_7, paste0(chemin_donnees_vacc, "episodes_a_7_jours_",mois_ciblage ,".csv"))


# episodes de la V2, à partir d'avril 2021

dt2_0 <- fread(paste0(chemin_data_v2, "all_epidemio_reports.csv"))
dt_history <- fread(paste0(chemin_data_v2, "historique.csv"))

# set types
dt_history[, date_deb_sign_premcas := as.Date(date_deb_sign_premcas)]
dt_history[, date_deb_sign_premcasc_ep := as.Date(date_deb_sign_premcas)]
dt_history[, date_deb_sign_dercas := as.Date(date_deb_sign_dercas)]
dt_history[, extract_date := as.Date(extract_date)]
dt_history[, date_creation := as.Date(date_creation)]
# nombre de jours depuis le début de l'épisode à la date d'extraction
dt_history[, nb_jours_deb := as.integer(extract_date - date_deb_sign_premcas)]

njours <- 14
# filtre ehpad et n jours
dt_final <- dt_history[nb_jours_deb == njours & sous_categorie_esms == "EHPAD" & date_deb_sign_premcas > as.Date("2021-04-01") & !region %in% c("COR", "MAR", "REU")]
print(paste0("Nombre d'épisodes en Ehpad avec un ", njours, "ème jour après le début des signes du premier cas depuis le 1er avril 2021 : ", nrow(dt_final)))

dt2 <- ajout_situation_vaccinale_episode(dt_final, dtvac_hist2, njrecul_ = njrecul) 

dt2 <- merge(dt2, dt_finess, by.x="finess", by.y="nofiness", all.x=T)
dt2[, finess := as.character(finess)]
dt2[, nb_total_residents := tot_cap]
dt2[, nb_conf_res_max := nb_residents_touches]
dt2[, nb_residents_dcd_tot := nb_residents_dcd_hosp + nb_residents_dcd_etab]
dt2[, nb_conf_perso_max := nb_conf_perso]

fwrite(dt2, paste0(chemin_donnees_vacc, "episodes_a_14_jours_v2_",mois_ciblage ,".csv"))


njours <- 21
# filtre ehpad et n jours
dt_final <- dt_history[nb_jours_deb == njours & sous_categorie_esms == "EHPAD" & date_deb_sign_premcas > as.Date("2021-04-01") & !region %in% c("COR", "MAR", "REU")]
print(paste0("Nombre d'épisodes en Ehpad avec un ", njours, "ème jour après le début des signes du premier cas depuis le 1er avril 2021 : ", nrow(dt_final)))

dt2 <- ajout_situation_vaccinale_episode(dt_final, dtvac_hist2, njrecul_ = njrecul) 

dt2 <- merge(dt2, dt_finess, by.x="finess", by.y="nofiness", all.x=T)
dt2[, finess := as.character(finess)]
dt2[, nb_total_residents := tot_cap]
dt2[, nb_conf_res_max := nb_residents_touches]
dt2[, nb_residents_dcd_tot := nb_residents_dcd_hosp + nb_residents_dcd_etab]
dt2[, nb_conf_perso_max := nb_conf_perso]

fwrite(dt2, paste0(chemin_donnees_vacc, "episodes_a_21_jours_v2_",mois_ciblage ,".csv"))


njours <- 28
# filtre ehpad et n jours
dt_final <- dt_history[nb_jours_deb == njours & sous_categorie_esms == "EHPAD" & date_deb_sign_premcas > as.Date("2021-04-01") & !region %in% c("COR", "MAR", "REU")]
print(paste0("Nombre d'épisodes en Ehpad avec un ", njours, "ème jour après le début des signes du premier cas depuis le 1er avril 2021 : ", nrow(dt_final)))

dt2 <- ajout_situation_vaccinale_episode(dt_final, dtvac_hist2, njrecul_ = njrecul) 

dt2 <- merge(dt2, dt_finess, by.x="finess", by.y="nofiness", all.x=T)
dt2[, finess := as.character(finess)]
dt2[, nb_total_residents := tot_cap]
dt2[, nb_conf_res_max := nb_residents_touches]
dt2[, nb_residents_dcd_tot := nb_residents_dcd_hosp + nb_residents_dcd_etab]
dt2[, nb_conf_perso_max := nb_conf_perso]

fwrite(dt2, paste0(chemin_donnees_vacc, "episodes_a_28_jours_v2_",mois_ciblage ,".csv"))
